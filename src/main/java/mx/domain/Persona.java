package mx.domain;

import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
/*Con esta notacion se manejan los get/set
de cada atributo, sin necesidad de escribirlo*/
@Entity
@Table(name = "persona")
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPersona;
    
    /*Con esta notacion se valida que no sea cadena vacia*/
    @NotEmpty
    private String nombre;
    
    @NotEmpty
    private String apellido;
    
    @NotNull
    private String email;
    
    private String telefono;
    
    
    
    
}
