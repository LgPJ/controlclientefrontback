package mx.servicio;

import java.util.List;
import mx.domain.Persona;
import mx.dao.IPersonaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
/*Con esta notacion es la unica forma de que spring
reconozca esta clase como un service
y asi poder inyectarla dentro del controlador*/
public class PersonaServicesImpl implements PersonaService{

    @Autowired
    private IPersonaDao personaDao;
    
    @Override
    @Transactional(readOnly = true)
    /*readOnly es porque solo se va a leer de la base de datos*/
    public List<Persona> listarPersonas() {
            return (List<Persona>) personaDao.findAll();
        
    }

    @Override
    @Transactional
    public void guardar(Persona persona) {
        personaDao.save(persona);
    }

    @Override
    @Transactional
    public void eliminar(Persona persona) {
        personaDao.delete(persona);
    }
    

    @Override
    @Transactional(readOnly = true)
    public Persona encontrarPersona(Persona persona) {
        return personaDao.findById(persona.getIdPersona()).orElse(null);
    }
    
}
