package mx.web;

import java.util.ArrayList;
import javax.validation.Valid;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.domain.Persona;
import mx.servicio.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@Slf4j
public class ControladorInicio {
        
    @Autowired
    /*con esta notacion se hace la inyeccion sin necesidad
    del xml y la notacion Inyect*/
    private PersonaService personaService;
    
    
    @GetMapping("/")
    public String inicio(Model model){
        
        List<Persona> personas;
        personas = new ArrayList();
        
        personas = (List<Persona>) personaService.listarPersonas();
        
        model.addAttribute("personas", personas);
        return "index";
        /*El index del return es el nombre del mismo documento html*/
    }
    
    @GetMapping("/agregar")
    public String agregar(Persona persona){
        
        return "modificar";
        
    }
    
    /*El path que esta definido aqui /guardar
    es el path que estad efinido en el html modificar
    @action=guardar
    el metodo es el definido en el html 
    method=post*/
    
    /*El valid es para validar el objeto 
    y el metodo Errors es para obtener los errores que pueden venir
    dentro del objeto*/
    @PostMapping("/guardar")
    public String guardar(@Valid Persona persona, Errors errores){
        if(errores.hasErrors()){
            return "modificar";
        }
        personaService.guardar(persona);
        
        /*Redireccionamiento a la pagina de inicio
        */
        return "redirect:/";
    }
    
    @GetMapping("/editar/{idPersona}")
    public String editar(Persona persona, Model model){
        persona = personaService.encontrarPersona(persona);
        model.addAttribute("persona", persona);
        return "modificar";
    }
    
    /*Al definir dentro del path
    la direccion /idPersona, spring
    obre entiende que debe insertar el valor del path dentro de la 
    variable definida del tipo persona*/
    @GetMapping("/eliminar/{idPersona}")
    public String eliminar(Persona persona){
        personaService.eliminar(persona);
        return "redirect:/";
    }
    
}
